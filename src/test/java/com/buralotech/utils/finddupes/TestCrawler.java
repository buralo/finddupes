package com.buralotech.utils.finddupes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestCrawler {

    private static final URL source1 = TestCrawler.class.getResource("/IgorKireev-1.jpg");

    private static final URL source2 = TestCrawler.class.getResource("/IgorKireev-2.jpg");

    private static final URL source3 = TestCrawler.class.getResource("/IgorKireev-3.jpg");

    @TempDir
    private Path root;

    private Path subdir;

    @BeforeEach
    void setup() throws IOException {
        subdir = root.resolve("subdir");
        Files.createDirectory(subdir);
        copyResourceToFile(source1, root.resolve("IgorKireev-1.jpg"));
        copyResourceToFile(source1, subdir.resolve("IgorKireev-1.jpg"));
        copyResourceToFile(source2, root.resolve("IgorKireev-2.jpg"));
        copyResourceToFile(source2, root.resolve("IgorKireev-2a.jpg"));
        copyResourceToFile(source3, root.resolve("IgorKireev-3.jpg"));
        copyResourceToFile(source3, root.resolve("IgorKireev-3a.jpg"));
        copyResourceToFile(source3, subdir.resolve("IgorKireev-3.jpg"));
    }

    @Test
    void crawlEmpty() {
        final var crawler = new Crawler();
        crawler.scanDirectory(root);
        crawler.process(Crawler::removeDuplicate);
        assertTrue(Files.exists(root.resolve("IgorKireev-1.jpg")));
        assertTrue(Files.exists(root.resolve("IgorKireev-2.jpg")));
        assertTrue(Files.exists(root.resolve("IgorKireev-3.jpg")));
        assertFalse(Files.exists(subdir.resolve("IgorKireev-1.jpg")));
        assertFalse(Files.exists(root.resolve("IgorKireev-2a.jpg")));
        assertFalse(Files.exists(root.resolve("IgorKireev-3a.jpg")));
        assertFalse(Files.exists(subdir.resolve("IgorKireev-3.jpg")));
    }

    private void copyResourceToFile(final URL source,
                                    final Path destination)
            throws IOException {
        try (final InputStream inputStream = new BufferedInputStream(source.openStream())) {
            Files.copy(inputStream, destination);
        }
    }
}
