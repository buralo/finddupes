package com.buralotech.utils.finddupes;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

public class Crawler {

    private static final Logger LOGGER = Logger.getLogger(Crawler.class.getName());

    private final ExecutorService executorService = Executors.newFixedThreadPool(4);

    private final ConcurrentHashMap<Hash, Collection<Path>> results = new ConcurrentHashMap<>();

    private final Phaser phaser = new Phaser();

    public Crawler() {
        phaser.register();
    }

    public static void reportDuplicate(final Path original, final Path copy) {
        System.out.printf("%s is duplicated by %s%n", original.toAbsolutePath(), copy.toAbsolutePath());
    }

    public static void removeDuplicate(final Path original, final Path copy) {
        try {
            Files.delete(copy);
            System.out.printf("%s was deleted%n", copy.toAbsolutePath());
        } catch (final IOException e) {
            LOGGER.warning("Error deleting file: " + copy.toAbsolutePath());
        }
    }

    public void scanDirectory(final Path path) {
        submit(new ScanDirectoryJob(path));
    }

    public void hashFile(final Path path) {
        submit(new HashFileJob(path));
    }

    private void submit(final Runnable job) {
        phaser.register();
        executorService.submit(job);
    }

    public void process(final BiConsumer<Path, Path> action) {
        phaser.arriveAndAwaitAdvance();
        executorService.shutdown();
        results.forEach((hash, paths) -> {
            if (paths.size() > 1) {
                final var iterator = paths.iterator();
                final var first = iterator.next();
                while (iterator.hasNext()) {
                    action.accept(first, iterator.next());
                }
            }
        });
    }

    public abstract class AbstractJob implements Runnable {

        protected final Path path;

        public AbstractJob(final Path path) {
            this.path = path;
        }

        @Override
        public void run() {
            doWork();
            phaser.arriveAndDeregister();
        }

        protected abstract void doWork();
    }

    public class HashFileJob extends AbstractJob {

        private static final int BUFFER_SIZE = 8192;

        public HashFileJob(final Path path) {
            super(path);
        }

        @Override
        public void doWork() {
            try {
                final var digest = MessageDigest.getInstance("SHA-256");
                try (
                        final InputStream is = Files.newInputStream(path);
                        final BufferedInputStream bis = new BufferedInputStream(is)) {
                    final var buffer = new byte[BUFFER_SIZE];
                    int count;
                    while ((count = bis.read(buffer)) > 0) {
                        digest.update(buffer, 0, count);
                    }
                }
                final var hash = new Hash(digest.digest());
                results.computeIfAbsent(hash, k -> new ConcurrentSkipListSet<>()).add(path);
            } catch (final NoSuchAlgorithmException | IOException e) {
                LOGGER.warning("Error hashing file: " + path.toAbsolutePath());
            }
        }
    }

    public class ScanDirectoryJob extends AbstractJob {

        public ScanDirectoryJob(final Path path) {
            super(path);
        }

        @Override
        public void doWork() {
            try {
                Files.walkFileTree(path, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
                        if (attrs.isRegularFile()) {
                            Crawler.this.hashFile(file);
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (final IOException e) {
                LOGGER.warning("Error scanning directory tree from: " +  path.toAbsolutePath());
            }
        }
    }

    public record Hash(byte[] bytes) {

        @Override
        public boolean equals(final Object other) {
            return (this == other) || (other instanceof Hash hash && Arrays.equals(bytes, hash.bytes));
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(bytes);
        }
    }
}
