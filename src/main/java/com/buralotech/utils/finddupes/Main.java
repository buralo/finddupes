package com.buralotech.utils.finddupes;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class Main {
    public static void main(final String[] args) {
        if (args.length > 0) {
            final Stream<String> arguments;
            final BiConsumer<Path, Path> action;
            if ("--dry-run".equalsIgnoreCase(args[0])) {
                arguments = Arrays.stream(args).skip(1);
                action = Crawler::removeDuplicate;
            } else {
                arguments =  Arrays.stream(args);
                action = Crawler::reportDuplicate;
            }
            final var crawler = new Crawler();
            arguments
                    .map(Path::of)
                    .filter(Files::exists)
                    .filter(Files::isDirectory)
                    .forEach(crawler::scanDirectory);
            crawler.process(action);
        }
    }
}
